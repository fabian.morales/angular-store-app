import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { PaginatedResponse } from '../interfaces/paginatedresponse';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getAllProducts() {
    return this.http.get<PaginatedResponse<Product>>('http://automotriz_valle.local/api/products');
  }

  getCategoryProducts(slug: string) {
    return this.http.get<PaginatedResponse<Product>>(`http://automotriz_valle.local/api/categories/${slug}/products`);
  }
}
