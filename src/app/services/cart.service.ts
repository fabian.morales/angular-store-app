import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CartItem } from '../models/cart-item';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private addedItems: CartItem[] = [];
  private addedItemsSubject = new BehaviorSubject<CartItem[]>([]);

  addItems$ = this.addedItemsSubject.asObservable();

  constructor() { }

  addItem(item: CartItem) {
    this.addedItems.push(item);
    this.addedItemsSubject.next(this.addedItems);
  }

  getTotalQuantity(): number {
    return this.addedItems.reduce((total, item) => total + item.quantity, 0);
  }
}
