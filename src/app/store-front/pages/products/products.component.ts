import { Component, OnInit } from '@angular/core';

import { Product } from '../../../models/product.model';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private productsService: ProductsService) { }

  products:Product[] = [];

  ngOnInit(): void {
    this.productsService.getAllProducts()
      .subscribe(response => this.products = response.data);
  }

}
