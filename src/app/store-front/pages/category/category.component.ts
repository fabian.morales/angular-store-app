import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';

import { Product } from '../../../models/product.model';
import { ProductsService } from '../../../services/products.service';

@Component({
  selector: 'category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private productsService: ProductsService
  ) { }

  categorySlug: string | null = '';
  products: Product[] = [];

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => {
        this.categorySlug = params.get('slug');
        return this.categorySlug ? this.productsService.getCategoryProducts(this.categorySlug) : [];
      })
    ).subscribe(response => this.products = response.data);
  }
}
