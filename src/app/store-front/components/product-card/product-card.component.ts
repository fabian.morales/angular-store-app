import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../models/product.model';
import { CartItem } from '../../../models/cart-item';
import { CartService } from '../../../services/cart.service';

@Component({
  selector: 'product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {
  constructor(
    private cartService: CartService
  ) { }
  
  @Input() product: Product = {
    id: 0,
    name: '',
    slug: '',
    sku: '',
    images: []
  };

  quantity:number = 0;

  onAddProduct() {
    const cartItem: CartItem = {
      product: this.product,
      quantity: this.quantity,
    }
    this.cartService.addItem(cartItem);
  }

  ngOnInit(): void {
  }

}
