import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'product-image',
  templateUrl: './product-image.component.html',
  styleUrls: ['./product-image.component.css']
})
export class ProductImageComponent implements OnInit {
  //@Input() url:string = ''; //Método simple
  @Output() imageLoaded = new EventEmitter<any>();

  url:string = '';
  @Input('url') //Método con setter
  set changeImageUrl(newUrl:string) {
    this.url = newUrl;
    
    //this.imageLoaded.emit(this.url);
    //console.log('emitido');
  }

  click() {
    this.imageLoaded.emit(this.url);
    console.log('emitido');
  }


  constructor() { }


  ngOnInit(): void {

  }


}
