import { Component, Input, OnInit } from '@angular/core';
import { Category } from '../../../models/category';
import { CategoriesService } from '../../../services/categories.service';

@Component({
  selector: 'nav-main-menu',
  templateUrl: './nav-main-menu.component.html',
  styleUrls: ['./nav-main-menu.component.css']
})
export class NavMainMenuComponent implements OnInit {

  constructor(private categoryService: CategoriesService) { }

  categories: Category[] = [];

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe(response => {
      this.categories = response.data;
    });
  }

}
