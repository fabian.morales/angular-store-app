import { Component, OnInit } from '@angular/core';

import { CartItem } from '../../../models/cart-item';
import { CartService } from '../../../services/cart.service';

@Component({
  selector: 'cart-basket',
  templateUrl: './cart-basket.component.html',
  styleUrls: ['./cart-basket.component.css']
})
export class CartBasketComponent implements OnInit {

  constructor(
    private cartService: CartService
  ) { }

  itemsQuantity: number = 0;
  totalPrice: number = 0;

  ngOnInit(): void {
    this.cartService.addItems$.subscribe(items => this.itemsQuantity = items.reduce((total:number, item:CartItem) => total + (+item.quantity), 0));
  }

}
