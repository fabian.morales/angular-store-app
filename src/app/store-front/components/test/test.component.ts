import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  user = {
    name: 'Fabian',
    age: 38,
    email: 'fabian.morales@outlook.com',
    password: 'abcd1234',
    team: 'Marvel',
  }

  teams:string[] = ['Marvel', 'DC', 'Otros'];

  buttonDisabled = false;

  increaseAge() {
    this.user.age += 1;
  }

  toggleButton() {
    this.buttonDisabled = !this.buttonDisabled;
  }

  onSubmit() {
    console.log(this.user);
  }

  alertar(url:string) {
    alert('recibido');
    console.log(url);
  }

}
