import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { StoreFrontRoutingModule } from './store-front-routing.module';

import { ProductImageComponent } from './components/product-image/product-image.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { TestComponent } from './components/test/test.component';
import { NavMainMenuComponent } from './components/nav-main-menu/nav-main-menu.component';
import { CartBasketComponent } from './components/cart-basket/cart-basket.component';

import { LayoutComponent } from './pages/layout/layout.component';
import { ProductsComponent } from './pages/products/products.component';
import { HomeComponent } from './pages/home/home.component';
import { CategoryComponent } from './pages/category/category.component';


@NgModule({
  declarations: [
    ProductImageComponent,
    ProductListComponent,
    ProductCardComponent,
    TestComponent,
    NavMainMenuComponent,
    CartBasketComponent,
    LayoutComponent,
    ProductsComponent,
    HomeComponent,
    CategoryComponent,
  ],
  imports: [
    CommonModule,
    StoreFrontRoutingModule,
    FormsModule,
  ]
})
export class StoreFrontModule { }
