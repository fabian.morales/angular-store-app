import { Image } from './image';

export interface Product {
    id: number;
    name: string;
    slug: string;
    sku: string;
    images: Image[];
}